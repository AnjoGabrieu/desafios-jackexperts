#!/usr/bin/bash

echo "content-type: text/html"
echo

echo "<html>"
echo "<head>"
echo "<meta charset=UTF-8>"
echo "</head>"
echo "<body>"

echo "<h1>Calculo simples!</h1>"

echo "<form method=GET action=\"${SCRIPT}\">"\
       '<table nowrap>'\
          '<tr><td>Primeiro número:</TD><TD><input type="text" name="a"></td></tr>'\
          '<tr><td>Segundo número:</td><td><input type="text" name="b" value=""></td>'\
          '</tr></table>'
echo '<br><input type="submit" value="Somar"><br>'

num1=`echo "$QUERY_STRING" | sed -n 's/^.*a=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`
num2=`echo "$QUERY_STRING" | sed -n 's/^.*b=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`
soma=$(($num1+$num2))
echo "<br>O resultado é: "
echo "<br>"$soma

echo "</form>"
echo "</body>"
echo "</html>"
echo "</body>"
echo "</html>"
