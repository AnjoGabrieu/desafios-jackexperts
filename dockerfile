FROM ubuntu:latest
RUN apt update -y && apt install -y apache2
RUN command a2enmod cgi
COPY ./projeto.cgi /usr/lib/cgi-bin
COPY ./ports.conf /etc/apache2/ports.conf
CMD ["-D" , "FOREGROUND"]
ENTRYPOINT ["apachectl"]
